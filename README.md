# Project's description

## 0. Introduction
This is an Android project to submit for 12th of May at the University of the Luxembourg. The concept is to display a **Shopping List** where you can INSERT / UPDATE an item from the list.
Due to the deadline, I wrote the code to deploy a `monolithic` App instead of a `multi-module` one.

## 1. Project Structure
```
AndroidUniShopping
│
├─ app
│  ├─ src   
│  │  ├─ main
│  │  │  └─ ...
│  │  │     ├─ adapter
│  │  │     │  └─ RecyclerAdapterItem.kt
│  │  │     │
│  │  │     ├─ pages
│  │  │     │  ├─ ActivityShoppingInsertItem.kt
│  │  │     │  └─ ActivityShoppingUpdateItem.kt
│  │  │     │
│  │  │     ├─ repository
│  │  │     │  └─ room
│  │  │     │     ├─ sqllite
│  │  │     │     │  ├─ dao
│  │  │     │     │  │  └─ Item.kt
│  │  │     │     │  │
│  │  │     │     │  ├─ entity
│  │  │     │     │  │  └─ Item.kt
│  │  │     │     │  │
│  │  │     │     │  └─ database
│  │  │     │     │     └─ Item.kt
│  │  │     │     │ 
│  │  │     │     └─ RepositoryItem.kt
│  │  │     │ 
│  │  │     ├─ ui
│  │  │     │  └─ ActivityShopping.kt
│  │  │     │  
│  │  │     ├─ utils
│  │  │     │  ├─ Constants.kt 
│  │  │     │  └─ RecyclerItemDecorator.kt
│  │  │     │  
│  │  │     ├─ viewmodels
│  │  │     │  └─ ItemViewModel.kt
│  │  │     │
│  │  │     ├─ MainActivity.kt
│  │  │     └─ SplashScreen.kt
│  │  │
│  │  ├─ res
│  │  │  ├─ ...
│  │  │  ├─ layout
│  │  │  │  ├─ activity_main.xml
│  │  │  │  ├─ activity_shopping.xml
│  │  │  │  ├─ activity_shopping_insert_item.xml
│  │  │  │  ├─ activity_shopping_recyclerview_item_row.xml
│  │  │  │  ├─ activity_shopping_update_item.xml
│  │  │  │  └─ activity_splash_screen.xml
│  │  │  │ 
│  │  │  └─ ...
│  │  └─ AndroidManifest.xml
│  │
│  └─ build.gradle
│
├─ ...
│
└─ README.md
```
