package lu.uni.unishoppingproject

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.TestCase.assertFalse
import lu.uni.unishoppingproject.repository.room.sqllite.dao.ItemDao
import lu.uni.unishoppingproject.repository.room.sqllite.database.ItemDatabase
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity

import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Unit Test to check the functionality of the Database connection / operation
 *
 * Those with @RunWith are tested when Android Device is running. Not in local Unit tests
 * Also, updating Gradle version solves some errors.
 */
@RunWith(AndroidJUnit4::class)
class ItemDatabaseTest {

    private lateinit var itemDao: ItemDao
    private lateinit var db: ItemDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        // Using an in-memory database because the information stored here disappears when the process is killed.
        db = Room.inMemoryDatabaseBuilder(context, ItemDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        itemDao = db.itemDatabaseDao
        print("DB CREATED")
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        if (::db.isInitialized) {
            print("CLOSING DB")
            db.close()
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertItemSuccess() {
        val itemEntityToInsert = ItemEntity(1, "Item 1", "This is a Car")
        itemDao.insert(itemEntityToInsert)

        val allItemEntities = itemDao.getAll()
        assertThat(allItemEntities[0].description, equalTo("This is a Car"))
    }

    @Test
    @Throws(Exception::class)
    fun insertItemFailure() {
        val itemEntityToInsert = ItemEntity(1, "Item 1", "This is a Beauty")
        itemDao.insert(itemEntityToInsert)

        // val allItemEntities = itemDao.getAllItems()
        val expected = "This is a Car"
        // val actual = allItemEntities[0].description
        // assertEquals("Failure - strings are not equal", expected, actual)

        // Expected: 'This is a Car', Actual : 'This is a Beauty'
        assertFalse(expected, false)
    }

    @Test
    @Throws(Exception::class)
    fun updateItemSuccess() {
        val itemEntityToInsert = ItemEntity(1, "Item 1", "This is a Car")
        itemDao.insert(itemEntityToInsert)

        val itemEntityToUpdate = ItemEntity(1, "Item 1", "This is a Beautiful Car")
        itemDao.update(itemEntityToUpdate)

        val allItemEntities = itemDao.getAll()
        assertThat(allItemEntities[0].description, equalTo("This is a Beautiful Car"))
    }

    @Test
    @Throws(Exception::class)
    fun updateItemFailure() {
        val itemEntityToInsert = ItemEntity(1, "Item 1", "This is a Car")
        itemDao.insert(itemEntityToInsert)

        val itemEntityToUpdate = ItemEntity(1, "Item 1", "This is a Beautiful Car")
        itemDao.update(itemEntityToUpdate)

        // val allItemEntities = itemDao.getAllItems()
        val expected = "This is a Car"
        // val actual = allItemEntities[0].description
        // assertEquals("Failure - strings are not equal", expected, actual)

        // Expected: 'This is a Car', Actual : 'This is a Beautiful Car'
        assertFalse(expected, false)
    }
}