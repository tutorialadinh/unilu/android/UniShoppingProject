package lu.uni.unishoppingproject

import android.R.attr.visible
import android.R.id.text2
import android.animation.LayoutTransition
import android.content.Intent
import android.icu.number.Scale
import android.os.Bundle
import android.view.View
import android.view.animation.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.transition.Fade
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import lu.uni.unishoppingproject.databinding.ActivityMainBinding
import lu.uni.unishoppingproject.ui.ActivityShopping


class MainActivity : AppCompatActivity() {
    /**
     * First Enable View Binding in build.gradle (app)
     * 'lateinit' is late initialization
     *
     * As we're using 'activity_main' layout then name should be Activity + Main+ Binding
     */
    private lateinit var binding: ActivityMainBinding

    /**
     * Method called when instantiating Activity
     *
     * We will bind Views
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Fade in / out animation
        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator() //add this
        fadeIn.duration = 1000

        val fadeOut = AlphaAnimation(1f, 0f)
        fadeOut.interpolator = AccelerateInterpolator() //and this
        fadeOut.startOffset = 1000
        fadeOut.duration = 1000

        val animation = AnimationSet(false) //change to false
        animation.addAnimation(fadeOut)
        animation.addAnimation(fadeIn)
        binding.mainWelcomeButton.animation = animation

        // Click event
        binding.mainWelcomeButton.setOnClickListener() {
            // Go to Shopping Activity and no return
            val intent = Intent(this, ActivityShopping::class.java)
            startActivity(intent)
            finish()
        }
    }
}