package lu.uni.unishoppingproject.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import lu.uni.unishoppingproject.R
import lu.uni.unishoppingproject.pages.ActivityShoppingUpdateItem
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity
import lu.uni.unishoppingproject.utils.Constants


/**
 * Adapter used to manage Item list displaying
 * https://medium.com/@imtripathishivam/insane-guide-recyclerview-with-kotlin-in-androidx-chapter-1-d8511d84d4bd
 */
class RecyclerAdapterItem(private val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {

    // Init the list as MutableList to add ItemEntity within it
    private var items = emptyList<ItemEntity>().toMutableList()

    // Gets the number of items in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.activity_shopping_recyclerview_item_row, parent, false)
        )
    }

    // Binds each item in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tag = items[position]

        // Set UI values
        holder.shoppingItemTitle.text = items[position].title

        // Set handlers
        holder.itemView.setOnClickListener {
            // Get ItemEntity object data
            val item = it.tag as ItemEntity

            // Go to Update page while transmitting ItemEntity id
            val intent = Intent(it.context, ActivityShoppingUpdateItem::class.java).apply {
                putExtra(Constants.ITEM_RECORD_ID, item.id)
            }
            it.context.startActivity(intent)
        }
    }

    /**
     * Method to populate the list
     */
    fun setItems(items: List<ItemEntity>) {
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }
}

// ==========================================================================================================

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val shoppingItemTitle: TextView = itemView.findViewById(R.id.tvItemTitle)
}