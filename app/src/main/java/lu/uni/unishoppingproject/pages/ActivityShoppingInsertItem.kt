package lu.uni.unishoppingproject.pages

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import lu.uni.unishoppingproject.databinding.ActivityShoppingInsertItemBinding
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity
import lu.uni.unishoppingproject.vm.ItemViewModel


/**
 * Page where we insert Item
 */
class ActivityShoppingInsertItem : AppCompatActivity() {

    /**
     * First Enable View Binding in build.gradle (app)
     * 'lateinit' is late initialization
     *
     * As we're using 'activity_shopping_insert_item' layout then name should be Activity + Shopping + Insert + Item + Binding
     */
    private lateinit var binding: ActivityShoppingInsertItemBinding

    private lateinit var itemViewModel: ItemViewModel


    /**
     * The Activity / Fragment is being created
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Binding
        binding = ActivityShoppingInsertItemBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Retrieve the viewModel
        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)


        // Button INSERT
        binding.buttonInsert.setOnClickListener {

            val id = 0L
            val recordTitle = binding.tfItemTitle.text.toString()
            val recordDescription = binding.tfItemDescription.text.toString()

            // Check if TextField empty
            if (recordTitle.isBlank() or recordTitle.isEmpty()) {
                Snackbar.make(it, "Title should not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()

                if (recordDescription.isBlank() or recordDescription.isEmpty()) {
                    Snackbar.make(it, "Description should not be empty", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            } else {
                if (recordDescription.isBlank() or recordDescription.isEmpty()) {
                    Snackbar.make(it, "Description should not be empty", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                } else {
                    val item =
                        ItemEntity(id = id, title = recordTitle, description = recordDescription)
                    itemViewModel.insert(item)

                    // Return to Shopping Activity and no return
                    finish()
                }
            }
        }
    }
}