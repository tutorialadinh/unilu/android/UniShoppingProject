package lu.uni.unishoppingproject.pages

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar

import lu.uni.unishoppingproject.databinding.ActivityShoppingUpdateItemBinding
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity
import lu.uni.unishoppingproject.utils.Constants
import lu.uni.unishoppingproject.vm.ItemViewModel

/**
 * Page where we update Item
 */
class ActivityShoppingUpdateItem : AppCompatActivity() {
    /**
     * First Enable View Binding in build.gradle (app)
     * 'lateinit' is late initialization
     *
     * As we're using 'activity_shopping_update_item' layout then name should be Activity + Shopping + Update + Item + Binding
     */
    private lateinit var binding: ActivityShoppingUpdateItemBinding

    private lateinit var itemViewModel: ItemViewModel

    /**
     * Variable used to store the Id from ItemEntity object
     */
    private var recordId: Long = 0L

    /**
     * The Activity / Fragment is being created
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Binding
        binding = ActivityShoppingUpdateItemBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Retrieve the viewModel
        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)

        // If we call ActivityShoppingUpdateItem from Adapter
        if (intent.hasExtra(Constants.ITEM_RECORD_ID)) {
            recordId = intent.getLongExtra(Constants.ITEM_RECORD_ID, 0L)

            /*
             * In this case, since the DAO `get(id)` method returns a `LiveData` object, we use the `observer` pattern to populate the view.
             */
            itemViewModel.get(recordId).observe(this, Observer {

                // Protect from null, which occurs when we delete the item
                if (it != null) {
                    // Populate with data
                    binding.tfItemTitle.setText(it.title)
                    binding.tfItemDescription.setText(it.description)
                }
            })
        }

        binding.buttonUpdate.setOnClickListener() {
            val id = recordId
            val recordTitle = binding.tfItemTitle.text.toString()
            val recordDescription = binding.tfItemDescription.text.toString()

            // Check if TextField empty
            if (recordTitle.isBlank() or recordTitle.isEmpty()) {
                Snackbar.make(it, "Title should not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()

                if (recordDescription.isBlank() or recordDescription.isEmpty()) {
                    Snackbar.make(it, "Description should not be empty", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            } else {
                if (recordDescription.isBlank() or recordDescription.isEmpty()) {
                    Snackbar.make(it, "Description should not be empty", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                } else {
                    val item =
                        ItemEntity(id = id, title = recordTitle, description = recordDescription)
                    itemViewModel.update(item)

                    // Return to Shopping Activity and no return
                    finish()
                }
            }
        }
    }
}