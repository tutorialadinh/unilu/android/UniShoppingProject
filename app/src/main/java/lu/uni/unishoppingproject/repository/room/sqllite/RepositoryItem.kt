package lu.uni.unishoppingproject.repository.room.sqllite

import androidx.lifecycle.LiveData
import lu.uni.unishoppingproject.repository.room.sqllite.database.ItemDatabase
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity

/**
 * The repository class gives the data to the ViewModel class and then the ViewModel class uses that data for Views.
 * All DB operations should be operated  in the background thread using Coroutine with Dispatchers.IO
 *
 * https://camposha.info/daily-tasks-planner-kotlin-room/
 * https://medium.com/androiddevelopers/coroutines-on-android-part-i-getting-the-background-3e0e54d20bb
 * https://www.geeksforgeeks.org/how-to-build-a-grocery-android-app-using-mvvm-and-room-database/
 */
class RepositoryItem(private val db: ItemDatabase) {

    val allItems: LiveData<List<ItemEntity>> = db.itemDatabaseDao.getAllAsync()

    fun get(id: Long): LiveData<ItemEntity> {
        return db.itemDatabaseDao.get(id)
    }

    suspend fun update(item: ItemEntity) {
        db.itemDatabaseDao.update(item)
    }

    suspend fun insert(item: ItemEntity) {
        db.itemDatabaseDao.insert(item)
    }

    suspend fun delete(item: ItemEntity) {
        db.itemDatabaseDao.delete(item)
    }

    suspend fun deleteAll() {
        db.itemDatabaseDao.deleteAll()
    }
}