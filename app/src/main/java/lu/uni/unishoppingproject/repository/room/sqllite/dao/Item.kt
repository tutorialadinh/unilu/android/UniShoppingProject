package lu.uni.unishoppingproject.repository.room.sqllite.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity

/**
 * DAO interface for ItemEntity. Used to define Access methods to any Databases
 */
@Dao
interface ItemDao {

    /**
     * Method used to retrieve all entries in the 'items' table
     */
    @Query("SELECT * FROM items")
    fun getAll(): List<ItemEntity>

    /**
     * Method used to retrieve all entries in the 'items' table in background
     * LiveData is an Observable data class
     *
     * https://proandroiddev.com/flow-livedata-what-are-they-best-use-case-lets-build-a-login-system-39315510666d
     */
    @Query("SELECT * FROM items")
    fun getAllAsync(): LiveData<List<ItemEntity>>

    /**
     * Method used to insert a specific entry in the 'items' table
     *
     * We declare the function as a suspending function to be used from a Coroutine (which replaces the deprecated AsyncTask).
     * Functions called in a Coroutine don't block the UI main thread.
     */
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(itemEntity: ItemEntity)

    /**
     * Method used to retrieve a specific entry in the 'items' table based on its id
     */
    @Query("SELECT * FROM items WHERE items.id = :id")
    fun get(id: Long): LiveData<ItemEntity>

    /**
     * Method used to update a specific entry in the 'items' table
     */
    @Update
    suspend fun update(vararg itemEntity: ItemEntity)

    /**
     * Method used to delete a specific entry in the 'items' table
     */
    @Delete
    suspend fun delete(vararg itemEntity: ItemEntity)

    /**
     * Method used to delete all entries in the 'items' table
     */
    @Query("DELETE FROM items")
    suspend fun deleteAll()
}