package lu.uni.unishoppingproject.repository.room.sqllite.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import lu.uni.unishoppingproject.repository.room.sqllite.dao.ItemDao
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity


/**
 * Class used to create the Database.
 * https://developer.android.com/codelabs/kotlin-android-training-room-database#5
 */
@Database(entities = [ItemEntity::class], version = 1, exportSchema = false)
abstract class ItemDatabase : RoomDatabase() {

    abstract val itemDatabaseDao: ItemDao

    /*
     * The companion object allows clients to access the methods for creating or getting the database without instantiating the class.
     * Since the only purpose of this class is to provide a database, there is no reason to ever instantiate it.
     *
     * Imagine it is a Singleton.
     */
    companion object {

        // Annotation used to declare this variable will never be cached and all writes and reads will be done to and from the main memory.
        @Volatile
        private var INSTANCE: ItemDatabase? = null

        // Method called from any classes to instantiate the DB
        fun getInstance(context: Context): ItemDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        ItemDatabase::class.java,
                        "item_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
