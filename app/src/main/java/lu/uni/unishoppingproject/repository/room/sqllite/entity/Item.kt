package lu.uni.unishoppingproject.repository.room.sqllite.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Item entity (which represents the 'Item' table in the database).
 * It is a Kotlin Data class. Its main purpose is to hold data.
 */
@Entity(tableName = "items")
data class ItemEntity(

    /**
     * Id of Shop item. Also PRIMARY KEY
     */
    @PrimaryKey(autoGenerate = true)
    val id: Long,

    /**
     * Title of Shop item
     */
    @ColumnInfo(name = "title")
    val title: String,

    /**
     * Item description.
     * Immutable variable (it means once instantiated, we can't modify it)
     */
    @ColumnInfo(name = "description")
    val description: String
)

