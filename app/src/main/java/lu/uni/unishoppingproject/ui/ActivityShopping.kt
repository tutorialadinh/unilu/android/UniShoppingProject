package lu.uni.unishoppingproject.ui

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import lu.uni.unishoppingproject.R
import lu.uni.unishoppingproject.adapter.RecyclerAdapterItem
import lu.uni.unishoppingproject.databinding.ActivityShoppingBinding
import lu.uni.unishoppingproject.pages.ActivityShoppingInsertItem
import lu.uni.unishoppingproject.utils.RecyclerItemDecorator
import lu.uni.unishoppingproject.vm.ItemViewModel


/**
 * Main Activity from shopping Module. Will be called by App Main Activity.
 */
class ActivityShopping : AppCompatActivity() {

    /**
     * First Enable View Binding in build.gradle (app)
     * 'lateinit' is late initialization
     *
     * As we're using 'activity_main' layout then name should be Activity + Main+ Binding
     */
    private lateinit var binding: ActivityShoppingBinding

    private lateinit var itemViewModel: ItemViewModel

    /**
     * Method called when instantiating Activity
     *
     * We will bind Views
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityShoppingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Link Layout Manager and Adapter to Recycler View + populate
        val recyclerView = binding.recyclerViewItems

        // Populate adapter with data
        val adapter = RecyclerAdapterItem(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Populate the list with Observer
        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)
        itemViewModel.allItems.observe(this, { items ->
            items?.let { adapter.setItems(it) }
        })

        // Swipe option to Recycler View
        setRecyclerViewItemTouchListener(recyclerView, this)

        // Add divider between views
        recyclerView.addItemDecoration(RecyclerItemDecorator(20, 20))


        // INSERT Button
        binding.buttonInsert.setOnClickListener() {
            // Call Insert Page
            val intent = Intent(this, ActivityShoppingInsertItem::class.java)
            startActivity(intent)
        }

        // DELETE ALL Button
        binding.buttonDeleteAll.setOnClickListener() {
            // Delete all
            itemViewModel.deleteAll()
        }
    }

    // =======================================================================================================================
    // =======================================================================================================================
    // =======================================================================================================================

    /**
     * Method used to swipe Item view and delete the record in the DB
     *
     * http://explorenshare.in/swipe-and-delete-recycler-view-item/
     */
    private fun setRecyclerViewItemTouchListener(recyclerView: RecyclerView, context: Context) {

        val deleteIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete)
        val intrinsicWidth = deleteIcon?.intrinsicWidth
        val intrinsicHeight = deleteIcon?.intrinsicHeight
        val background = ColorDrawable()
        val backgroundColor = Color.parseColor("#f44336")
        val clearPaint = Paint().apply { xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR) }

        //1
        val itemTouchCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                viewHolder1: RecyclerView.ViewHolder
            ): Boolean {
                //2
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //3
                val position = viewHolder.adapterPosition
                itemViewModel.allItems.value?.get(position)?.let { itemViewModel.delete(it) }
                recyclerView.adapter!!.notifyItemRemoved(position)
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {

                val itemView = viewHolder.itemView
                val itemHeight = itemView.bottom - itemView.top
                val isCanceled = dX == 0f && !isCurrentlyActive

                if (isCanceled) {
                    clearCanvas(
                        c,
                        itemView.right + dX,
                        itemView.top.toFloat(),
                        itemView.right.toFloat(),
                        itemView.bottom.toFloat()
                    )
                    super.onChildDraw(
                        c,
                        recyclerView,
                        viewHolder,
                        dX,
                        dY,
                        actionState,
                        isCurrentlyActive
                    )
                    return
                }

                // Draw the red delete background
                background.color = backgroundColor
                background.setBounds(
                    itemView.right + dX.toInt(),
                    itemView.top,
                    itemView.right,
                    itemView.bottom
                )
                background.draw(c)

                // Calculate position of delete icon
                val deleteIconTop = itemView.top + (itemHeight - intrinsicHeight!!) / 2
                val deleteIconMargin = (itemHeight - intrinsicHeight) / 2
                val deleteIconLeft = itemView.right - deleteIconMargin - intrinsicWidth!!
                val deleteIconRight = itemView.right - deleteIconMargin
                val deleteIconBottom = deleteIconTop + intrinsicHeight

                // Draw the delete icon
                deleteIcon.setBounds(
                    deleteIconLeft,
                    deleteIconTop,
                    deleteIconRight,
                    deleteIconBottom
                )
                deleteIcon.draw(c)

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }

            private fun clearCanvas(
                c: Canvas?,
                left: Float,
                top: Float,
                right: Float,
                bottom: Float
            ) {
                c?.drawRect(left, top, right, bottom, clearPaint)
            }

        }

        //4
        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }
}