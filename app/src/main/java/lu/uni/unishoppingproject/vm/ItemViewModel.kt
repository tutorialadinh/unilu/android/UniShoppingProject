package lu.uni.unishoppingproject.vm

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import lu.uni.unishoppingproject.repository.room.sqllite.RepositoryItem
import lu.uni.unishoppingproject.repository.room.sqllite.database.ItemDatabase
import lu.uni.unishoppingproject.repository.room.sqllite.entity.ItemEntity


/**
 * ViewModel class is used as an interface between View and Data.
 * It inherits from View Model class and we will create an instance variable of Repository.
 *
 * AndroidViewModel is subclass of ViewModel.
 * The Difference between them is we can pass Application Context which can be used whenever Application Context is required for example to instantiate Database in Repository.
 *
 * https://www.geeksforgeeks.org/how-to-build-a-grocery-android-app-using-mvvm-and-room-database/
 */
class ItemViewModel(application: Application) : AndroidViewModel(application) {
    //
    private val repository: RepositoryItem
    val allItems: LiveData<List<ItemEntity>>

    init {
        repository = RepositoryItem(ItemDatabase.getInstance(application))
        allItems = repository.allItems
    }

    // In coroutines thread insert item in insert function.
    fun insert(item: ItemEntity) = viewModelScope.launch {
        repository.insert(item)
    }

    // In coroutines thread update item in update function.
    fun update(item: ItemEntity) = viewModelScope.launch {
        repository.update(item)
    }

    // In coroutines thread delete item in delete function.
    fun delete(item: ItemEntity) = viewModelScope.launch {
        repository.delete(item)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }

    fun get(id: Long) = repository.get(id)
}